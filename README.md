# KMeans

## 01. Предисловие

- Чтобы начать, необходимо скачать [Jupyter Notebook и Python 3.8](https://www.youtube.com/watch?v=VIaur9G-0tc), а также посмотреть [гайд по основным моментам программы](https://www.istocks.club/%D0%BD%D0%B0%D1%87%D0%B0%D0%BB%D0%BE-%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%8B-%D1%81-jupyter-notebook-%D1%83%D1%87%D0%B5%D0%B1%D0%BD%D0%BE%D0%B5-%D0%BF%D0%BE%D1%81%D0%BE%D0%B1%D0%B8%D0%B5/2020-12-01/#:~:text=%D0%A7%D1%82%D0%BE%D0%B1%D1%8B%20%D0%B8%D0%BC%D0%BF%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D1%82%D1%8C%20%D1%84%D0%B0%D0%B9%D0%BB%20%D0%B2%20Jupyter,%D0%B5%D0%B3%D0%BE%20%D0%B2%20%D0%BA%D0%B0%D1%82%D0%B0%D0%BB%D0%BE%D0%B3%20%D1%80%D0%BE%D0%B4%D0%B8%D1%82%D0%B5%D0%BB%D1%8C%D1%81%D0%BA%D0%BE%D0%B9%20%D0%BF%D0%B0%D0%BF%D0%BA%D0%B8), чтобы запустить код. В случае безуспешных попыток, воспользуйтесь инструментарием [Google Colaboratory](https://colab.research.google.com/?hl=ru) и посмотрите пункт №3 или обратите внимание на пункт №4.
- Необходимо скачать все файлы [отсюда](https://drive.google.com/drive/folders/112kx0HFY24T55qIH-MUZliNcQngU8UIk?usp=sharing);

## 02. Начало работы в Jupyter Notebook

- Далее запустить ipynb-файл посредством Jupyter Notebook;
- Подгрузить все остальные файлы с расширением .xlsx в пространство Jupyter Notebook;
- Запустить все ячейки кода.

## 03. Начало работы в Google Colaboratory

- Необходимо создать свой блокнот, перейдя в [Google Colaboratory](https://colab.research.google.com/?hl=ru) и подгрузив файл KMeans.ipynb, установленный с первого пункта;
- Далее слева будет панель навигации по проекту (крюки для перехода между пунктами программы) - содержание. Перейти там, слева, во вкладку "Файлы" (нарисованная папочка), перетащить скачанные датасеты .xlsx в это небольшое поле;
- Сверху всей страницы на панели: Среда выполнения -> Выполнить все.

## 04. Обратная связь 

В случае безуспешных попыток запуска проекта, напишите мне на один из сервисов:
- Discord: Hecfi#4004;
- Vk: https://vk.com/hecfi;
- Kwork: https://kwork.ru/user/ruslangulyanovlive;
- Gmail: ruslangulyanov.live@gmail.com.